package com.army.web;


import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HelloWorldController {

    @PostMapping("/hello")
    public String hello(String name) {
        return "Hello World " + name;
    }

}
